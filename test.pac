function FindProxyForURL(url, host) {
  var useSpplSocks = [
    'confluence.sgpoolz.com.sg',
    'jira.sgpoolz.com.sg',
    '10.168.15.178',
    '10.208.15.154',
    '10.208.15.159',
    '10.168.15.187',
    '10.168.15.187',
    '10.168.15.199',
    '10.208.15.192',
    '10.208.15.194:8090',
    '10.208.15.194',
    '10.208.15.209',
    '10.208.15.210',
    '10.208.15.50',
  ];

  for (var i = 0; i < useSpplSocks.length; i++) {
    if (shExpMatch(host, useSpplSocks[i])) {
      return "SOCKS 61.8.238.234:4343";
    }
  }

  return "DIRECT";
}